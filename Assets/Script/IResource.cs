﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Script
{
    public interface IResource
    {
        event Action<float> onValueChanged;

        float CurrentValue { get; }

        float Add(float amount);
        float Remove(float amount);
    }
}
