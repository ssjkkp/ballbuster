﻿using Assets.Script;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHud : MonoBehaviour
{
    public Text playerHealthText;
    public Text playerScoreText;
    private IResource playerHealth;
    private IResource playerScore;

    private void Start()
    {
        GameObject player = GameObject.FindWithTag("Player");
        PlayerManager playerManager = player.GetComponent<PlayerManager>();
        playerHealth = playerManager.health;
        playerHealth.onValueChanged += OnHealthValueChanged;
        OnHealthValueChanged(playerHealth.CurrentValue);
        playerScore = playerManager.score;
        playerScore.onValueChanged += OnScoreValueChanged;
        OnScoreValueChanged(playerScore.CurrentValue);
    }

    private void OnHealthValueChanged(float amount)
    {
        playerHealthText.text = string.Format("Health: {0}", amount);
    }
    private void OnScoreValueChanged(float amount)
    {
        playerScoreText.text = string.Format(amount.ToString());
    }
}
