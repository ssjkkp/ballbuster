﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    [SerializeField] private Button scene1Button;
    [SerializeField] private Button scene2Button;
    [SerializeField] private Button optionsButton;
    [SerializeField] private Button quitButton;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    private void OnEnable()
    {
        scene1Button.onClick.AddListener(Scene1);
        scene2Button.onClick.AddListener(Scene2);
        //optionsButton.onClick.AddListener(Options);
        quitButton.onClick.AddListener(Quit);

    }

    private void Scene1()
    {
        SelectScene("AlternativeMode");
    }
    private void Scene2()
    {
        SelectScene("EndlessMode");
    }
    //private void Options()
    //{
    //    SelectScene("Options");
    //}
    private void Quit()
    {
        Application.Quit();
    }
    public void SelectScene(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }
}
