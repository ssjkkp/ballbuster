﻿using Assets.Script;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBehaviour : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            IDamageable damageable = other.GetComponent<IDamageable>();
            if (damageable != null)
            {
                damageable.Damage(1);
                if (!GameMenuController.GameIsPaused && !GameMenuController.GameIsOver)
                {
                    CameraShake shaker = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraShake>();
                    StartCoroutine(shaker.Shake(0.15f, 0.4f));
                }
            }
        }
    }
}
