﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InputController : MonoBehaviour
{
    //public static bool GameIsPaused = false;
    private GameAssets assets;
    public static System.Random random = new System.Random();

    void Start()
    {
        assets = FindObjectOfType<GameAssets>();
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !GameMenuController.GameIsPaused && !GameMenuController.GameIsOver)
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.gameObject.tag == "Ball")
                {
                    hit.transform.gameObject.SetActive(false);
                    assets.PopSounds[random.Next(0,assets.PopSounds.Length-1)].audio.Play();
                    Destroy(hit.transform.gameObject,3f);

                    GameObject player = GameObject.FindWithTag("Player");
                    if (player != null)
                    {
                        var manager = player.GetComponent<PlayerManager>();
                        manager.score.Add(1);
                    }
                }
                if (hit.transform.gameObject.tag == "Heart")
                {
                    hit.transform.gameObject.SetActive(false);
                    assets.PopSounds[random.Next(0, assets.PopSounds.Length - 1)].audio.Play();
                    Destroy(hit.transform.gameObject, 3f);

                    GameObject player = GameObject.FindWithTag("Player");
                    if (player != null)
                    {
                        var manager = player.GetComponent<PlayerManager>();
                        manager.Heal(1f);
                    }
                }
            }
        }
    }
}
