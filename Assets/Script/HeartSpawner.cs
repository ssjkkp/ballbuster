﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartSpawner : MonoBehaviour
{
    public float spawnTimeDebug;
    public float lifeTime;
    public int forceMax;
    public int forceMin;
    public int forwardForce;

    private GameAssets assets;
    private float time;
    private float spawnTime;
    public static System.Random random = new System.Random();

    void Start()
    {
        assets = FindObjectOfType<GameAssets>();
        SetRandomTime();
        time = spawnTimeDebug;
    }
    void FixedUpdate()
    {
        time += Time.deltaTime;

        if (time >= spawnTime)
        {
            SpawnObject();
            SetRandomTime();
        }
    }

    void SpawnObject()
    {
        time = spawnTimeDebug;
        var ball = Instantiate(assets.heart, transform.position, assets.heart.transform.rotation);
        var randX = random.Next(-forceMax, forceMax);
        var randY = forwardForce;
        var randZ = random.Next(-forceMax, forceMax);

        ball.GetComponent<Rigidbody>().AddForce(randX, -randY, randZ, ForceMode.Impulse);
        Destroy(ball, lifeTime);
    }

    void SetRandomTime()
    {
        spawnTime = spawnTimeDebug+10f;
    }
}
