﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAssets : MonoBehaviour
{
    public GameObject ball;
    public GameObject heart;
    public GameObject heart_hud;
    public GameObject poop;

    //public AudioSource ballPops;
    public PopAudioSource[] PopSounds;
    [System.Serializable]
    public class PopAudioSource
    {
        public AudioSource audio;
    }
    public PopAudioSource[] SpawnSounds;
    [System.Serializable]
    public class SpawnAudioSource
    {
        public AudioSource audio;
    }
}
