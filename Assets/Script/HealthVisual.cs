﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthVisual : MonoBehaviour
{

    public GameObject heart1;
    public GameObject heart2;
    public GameObject heart3;
    private int hearts=3;
    private GameAssets assets;

    //Refactor these a lot later lol
    public void AddHeart()
    {
        if (hearts == 0)
        {
            hearts++;
            heart1.SetActive(true);
        }
        else if (hearts==1)
        {
            hearts++;
            heart2.SetActive(true);
        }
        else if (hearts == 2)
        {
            hearts++;
            heart3.SetActive(true);
        }
    }
    public void RemoveHeart()
    {
        if (hearts == 1)
        {
            hearts--;
            heart1.SetActive(false);
            GameMenuController.GameIsOver = true;
        }
        else if (hearts == 2)
        {
            hearts--;
            heart2.SetActive(false);
        }
        else if (hearts == 3)
        {
            hearts--;
            heart3.SetActive(false);
        }
        //Debug.Log(hearts+" 1: "+ heart1.active+ " 2: " + heart2.active+" 3: " + heart3.active);
    }
}
