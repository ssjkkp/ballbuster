﻿using Assets.Script;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour, IDamageable
{
    public IResource health;
    public IResource score;
    private HealthVisual healthVisual;
    //public GameObject heart;

    private void Awake()
    {
        healthVisual = FindObjectOfType<HealthVisual>();
        health = new Health(3);
        score = new Score(0);
    }

    public void Damage(float damage)
    {
        health.Remove(damage);
        healthVisual.RemoveHeart();
    }
    public void Heal(float heal)
    {
        health.Add(heal);
        healthVisual.AddHeart();
    }
}
