﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public float maxTime;
    public float minTime;
    public float lifeTime;
    public int forceMax;
    public int forceMin;
    public bool directionIsStraight;

    private GameAssets assets;
    private float time;
    private float spawnTime;
    public static System.Random random = new System.Random();

    void Start()
    {
        assets = FindObjectOfType<GameAssets>();
        SetRandomTime();
        time = minTime;
    }
    void FixedUpdate()
    {
        time += Time.deltaTime;

        if (time >= spawnTime)
        {
            SpawnObject();
            SetRandomTime();
        }
    }

    void SpawnObject()
    {
        time = minTime;
        var item = Instantiate(assets.ball, transform.position, assets.ball.transform.rotation);
        //assets.SpawnSounds[random.Next(0, assets.SpawnSounds.Length - 1)].audio.Play();
        if (!directionIsStraight) { 
            var randX = random.Next(-forceMax, forceMax);
            var randY = random.Next(forceMin, forceMax);
            var randZ = random.Next(-forceMax, forceMax);
            item.GetComponent<Rigidbody>().AddForce(randX, -randY, randZ, ForceMode.Impulse);
        }
        Destroy(item, lifeTime);
    }

    void SetRandomTime()
    {
        spawnTime = Random.Range(minTime, maxTime);
    }
}
