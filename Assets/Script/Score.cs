﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Script
{
    public class Score : IResource
    {
        private float currentScore;

        public event Action<float> onValueChanged;

        public float CurrentValue
        {
            get { return currentScore; }
        }

        public Score(float initialValue)
        {
            currentScore = initialValue;
        }

        public float Add(float amount)
        {
            currentScore += amount;
            if (onValueChanged != null)
            {
                onValueChanged.Invoke(currentScore);
            }

            return currentScore;
        }

        public float Remove(float amount)
        {
            currentScore -= amount;
            if (onValueChanged != null)
            {
                onValueChanged.Invoke(currentScore);
            }

            return currentScore;
        }
    }
}
